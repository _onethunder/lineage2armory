import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';
import { ItemViewerComponent } from './item-viewer/item-viewer.component';
import { SkillViewerComponent } from './skill-viewer/skill-viewer.component';
import { SharedModule } from '../../shared/shared.module';


@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    HttpModule
  ],
  declarations: [ItemViewerComponent, SkillViewerComponent],
  exports: [ItemViewerComponent]
})
export class ViewerModule { }
