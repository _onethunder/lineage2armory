import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SkillViewerComponent } from './skill-viewer.component';

describe('SkillViewerComponent', () => {
  let component: SkillViewerComponent;
  let fixture: ComponentFixture<SkillViewerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SkillViewerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SkillViewerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
