import { Component, OnInit } from '@angular/core';
import { ApiItemsService } from '../../../shared/services/api-items.service';


@Component({
  selector   : 'app-item-viewer',
  templateUrl: './item-viewer.component.html',
  styleUrls  : [ './item-viewer.component.sass' ],
  providers : [ApiItemsService]
})
export class ItemViewerComponent implements OnInit {
  
  weapons: Array<any> = [];
  
  constructor(private apiItemsService: ApiItemsService) {
  }
  
  ngOnInit() {
    
    this.apiItemsService.getItems().subscribe(
      data => {
        this.weapons = data.weapons;
      },
      error => error,
      () => {
        console.log('done');
      });
    
  }
  
}
