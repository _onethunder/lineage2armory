import { TestBed, inject } from '@angular/core/testing';

import { ApiItemsService } from './api-items.service';

describe('ApiItemsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ApiItemsService]
    });
  });

  it('should be created', inject([ApiItemsService], (service: ApiItemsService) => {
    expect(service).toBeTruthy();
  }));
});
