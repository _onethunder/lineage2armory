import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class ApiItemsService {

  constructor(private http: Http) { }
  
  
  getItems() {
    return this.http.get('/api/items').map((res: Response) => res.json());
  }
  

}
