import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ItemGridComponent } from './components/item-grid/item-grid.component';
import { ItemCellComponent } from './components/item-cell/item-cell.component';
import { ApiItemsService } from './services/api-items.service';


@NgModule({
  imports     : [
    CommonModule
  ],
  declarations: [ ItemGridComponent, ItemCellComponent ],
  exports     : [ ItemGridComponent, ItemCellComponent ],
  providers   : [ ApiItemsService ]
})
export class SharedModule {
}
