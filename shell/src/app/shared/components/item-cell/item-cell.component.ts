import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-item-cell',
  templateUrl: './item-cell.component.html',
  styleUrls: ['./item-cell.component.sass']
})
export class ItemCellComponent implements OnInit {
  
  @Input()
  cellWidth;
  
  @Input()
  cellHeight;
  
  @Input()
  cellIcon;
  
  @Input()
  isEmpty;
  
  
  constructor() { }

  ngOnInit() {
  }

}
