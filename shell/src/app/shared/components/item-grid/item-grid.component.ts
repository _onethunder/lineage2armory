import { Component, Input, OnInit } from '@angular/core';


@Component({
  selector   : 'app-item-grid',
  templateUrl: './item-grid.component.html',
  styleUrls  : [ './item-grid.component.sass' ]
})
export class ItemGridComponent implements OnInit {
  
  @Input()
  gridItems: Array<any> = [];
  
  @Input()
  gridType: string;
  
  @Input()
  gridFlow: string;
  
  @Input()
  gridCellSize: number;
  
  @Input()
  gridCellMaxCount: number;
  
  @Input()
  gridStaticColumns: number;
  
  @Input()
  gridOrientation: string;
  
  /*Rewrite this*/
  gridSettings = {
    hasEmptyCells : false,
    gridFlow: {
      isFluid : false,
      isStatic : false,
    },
    gridOrientation: {
      isVertical: false,
      isHorizontal: false
    },
    staticColumns: 1
  };
  
  constructor() {
  }
  
  ngOnInit() {
    this.setGridSettings();
  }
  
  private setGridSettings() {
    switch (this.gridType) {
      case 'list' : {
        this.gridSettings.hasEmptyCells = false;
        break;
      }
  
      case 'container' : {
        this.gridSettings.hasEmptyCells = true;
        break;
      }
    }
  
    switch (this.gridFlow) {
      case 'fluid' : {
        this.gridSettings.gridFlow.isFluid = true;
        this.gridSettings.gridFlow.isStatic = false;
        break;
      }
    
      case 'static' : {
        this.gridSettings.gridFlow.isFluid = false;
        this.gridSettings.gridFlow.isStatic = true;
        break;
      }
    }
  
    switch (this.gridOrientation) {
      case 'h' : {
        this.gridSettings.gridOrientation.isHorizontal = true;
        this.gridSettings.gridOrientation.isVertical = false;
        break;
      }
    
      case 'v' : {
        this.gridSettings.gridOrientation.isHorizontal = false;
        this.gridSettings.gridOrientation.isVertical = true;
        break;
      }
    }
  }
  
  getMaxCells() {
    return new Array(this.gridCellMaxCount - this.gridItems.length);
  }
  
  getStaticColumns() {
    return this.gridCellSize * this.gridStaticColumns;
  }
  
}
