const express = require('express');
const router = express.Router();

/* GET api listing. */
router.get('/', (req, res) => {
  res.send('api works');
});

router.get('/items', (req, res) => {
  res.send({weapons: [

    {
      id: 1,
      icon: 'a_1'
    },
    {
      id: 2,
      icon: 'a_2'
    }

  ]});
});

module.exports = router;
